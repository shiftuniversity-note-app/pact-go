FROM golang:1.17

RUN curl -fsSL https://raw.githubusercontent.com/pact-foundation/pact-ruby-standalone/master/install.sh | bash
RUN cp -r pact /opt
